module.exports = function switchMap(fn) {
  return function switchMapOperator(outerRead) {
    var innerRead;
    var outerEnd;
    return function readable(abort, cb) {
      // abort the current stream, and then abort the stream of streams
      if (abort) {
        if (innerRead) {
          innerRead(abort, function(err) {
            outerRead(err || abort, cb);
          });
        } else {
          outerRead(abort, cb);
        }
      } else if (innerRead) {
        pullInner();
      } else {
        pullOuter();
      }

      function pullInner() {
        innerRead(null, function(err, data) {
          if (err === true) {
            if (outerEnd) return cb(outerEnd);
            else innerRead = null;
          } else if (err) {
            outerRead(true, function(abortErr) {
              // TODO: what do we do with the abortErr?
              cb(err);
            });
          } else cb(null, data);
        });
      }

      function pullOuter() {
        outerRead(null, function(end, x) {
          if (end) {
            if (end !== true) cb(end);
            else if (innerRead) outerEnd = end;
            else cb(end);
            return;
          }
          if (innerRead) {
            innerRead(true, function(err) {
              if (err && err !== true) cb(err);
            });
          }
          innerRead = fn(x);
          pullInner();
          pullOuter();
        });
      }
    };
  };
};
