# `pull-switch-map`

```
npm install --save pull-switch-map
```

The pull-stream equivalent to [RxJS's switchMap operator](https://www.learnrxjs.io/learn-rxjs/operators/transformation/switchmap).

## Description

In other words, this is essentially a *map-and-flatten* operation, but treats concurrency so that the outer stream is pulled concurrently *at the same time* as the inner stream is pulled, and once a new outer value is received, the current inner stream is aborted and swapped with the new inner stream.

Marble diagram:

```text
           interval(1000)
------------0------------1------------2------------3--------...

      switchMap(x => interval(350))

---------------0---1---2----0---1---2----0---1---2----0---1-...
```

Arguments to `switchMap(fn)`:

- `fn`: the mapping function that takes the value from the outer pull-stream and returns a new (inner) pull-stream

## Usage

```js
const pull = require('pull-stream')
const switchMap = require('pull-switch-map')

pull(
  somesource(),
  switchMap(x => pull.values([x])),
  pull.drain(i => console.log(i))
)
```

## License

MIT
