const test = require('tape');
const pull = require('pull-stream');
const switchMap = require('./index');

function interval(period) {
  let timeout;
  let i = 0;
  return function readable(end, cb) {
    if (end) {
      clearTimeout(timeout);
      timeout = void 0;
      cb(end);
      return;
    }
    timeout = setTimeout(() => {
      cb(null, i++);
    }, period);
  };
}

test('can cancel previous inner stream', function(t) {
  t.plan(9);

  let expected = [0, 1, 2, 10, 11, 12, 13, 14];
  let start = Date.now();
  pull(
    interval(350),
    pull.map(i => i * 10),
    pull.take(2),
    switchMap(i =>
      pull(
        interval(100),
        pull.map(x => i + x),
        pull.take(5),
      ),
    ),
    pull.drain(
      x => {
        console.log('-> got', x, 'at timestamp', Date.now() - start);
        const e = expected.shift();
        if (typeof e === 'number') {
          t.equal(x, e, 'emission');
        } else {
          t.fail(undefined, e, 'e should be defined', '=');
        }
      },
      () => {
        t.equal(expected.length, 0, 'all emissions done');
        t.end();
      },
    ),
  );
});
